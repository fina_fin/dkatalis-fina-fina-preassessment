import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class SpringBootProblem15 {
    public static void main(String[] args) {
        ChronoUnit unit = ChronoUnit.DAYS;
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2018-09-25T02:10:24+07:00");
        ZonedDateTime zonedDateTime2 = ZonedDateTime.parse("2019-08-25T02:10:24+07:00");
        System.out.println(zonedDateTimeDifference(zonedDateTime, zonedDateTime2, unit));
    }

    static long zonedDateTimeDifference(ZonedDateTime d1, ZonedDateTime d2, ChronoUnit unit){
        return unit.between(d1, d2);
    }
}
