@Getter
@Setter
@Entity
@Table(name = "ADDRESS")
public class Address {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    protected String id;

    @Column(name = "building")
    protected String building;

    @Column(name = "street")
    protected String street;

    @Column(name = "zipcode")
    protected String zipcode;

    @OneToOne
    @JoinColumn(name = "fk_restaurants")
    private Restaurants restaurants;
}