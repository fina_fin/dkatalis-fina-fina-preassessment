@Getter
@Setter
@Entity
@Table(name = "RESTAURANTS")
public class Restaurants {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    protected String id;

    @Column(name = "name")
    protected String name;

    @OneToOne
    @JoinColumn(name = "address_id")
    protected Address address;

    @Column(name = "borough")
    protected String borough;

    @Column(name = "cuisine")
    protected String cuisine;

    @OneToMany(mappedBy = "restaurants", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Grade> gradeList;


}