@Getter
@Setter
@Entity
@Table(name = "GRADE")
public class Grade {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    protected String id;

    @Column(name = "date")
    protected Date date;

    @Column(name = "grade")
    protected String grade;

    @Column(name = "score")
    protected Integer score;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_restaurants")
    private Restaurants restaurants;
}