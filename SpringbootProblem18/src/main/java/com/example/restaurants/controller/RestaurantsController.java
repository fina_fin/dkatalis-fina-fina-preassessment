import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

@RestController
@Api(tags = "repayment", produces = "application/json")
@RequestMapping(value = "/restaurants", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestaurantsController {

    @Autowired
    private RestaurantsService restaurantsService;

    @GetMapping("/top-five")
    public ResponseEntity<ResponseModel<Map<String, Object>>> findTopFiveRestaurants() throws ApplicationException {
        Map<String, Object> resultMap = restaurantsService.findTopFiveRestaurants();
        return ResponseEntity.ok(new ResponseModel<>(CommonConstants.MSG_SUCCESS, null, resultMap));
    }

}