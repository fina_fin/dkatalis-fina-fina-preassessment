import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantsRepository extends JpaRepository<Restaurants, String> {

    @Query(value = "select * from RESTAURANTS r innner join GRADE g on g.fk_restaurants = r.id order by g.score limit 5 ", nativeQuery = true)
    List<Restaurants> findTopFiveRestaurants();

}