public class RestaurantsService {

    @Autowired
    private RestaurantsRepository restaurantsRepository;

    public Map<String, Object> findTopFiveRestaurants() throws ApplicationException {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            List<Restaurants> restaurantsList = restaurantsRepository.findTopFiveRestaurants();
            List<Map<String, Object>> restaurantMapList = new ArrayList<>();
            for (Restaurants restaurants : restaurantsList) {
                Map<String, Object> restaurantMap = new HashMap<>();
                restaurantMap.put(ApplicationConstants.STR_ID, restaurants.getId());
                restaurantMap.put("restaurantsName", restaurants.getName());
                restaurantMapList.add(restaurantMap);
            }
            resultMap.put(CommonConstants.RESULT, restaurantMapList);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
        return resultMap;
    }
}