public class SpringBootProblem16 {
    interface N{
        int c(int n);
    }
    static N n = n->{
        int sum = 0, prime = 2, temp, x;
        for(;n > 0; sum += temp > 1 ? temp+0 *n--:0)
            for(temp = prime++, x = 2; x < temp; temp = temp % x++ < 1 ? 0 : temp);
        return sum;
    };

    public static void main(String[] args) {
        System.out.println(n.c(5));
    }
}